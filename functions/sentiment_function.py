import nltk
nltk.download('stopwords')
nltk.download('punkt')
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from textblob import TextBlob
import pickle

class SentimentFunctions:

    def __init__(self, file):
        self.file = file

    @staticmethod
    def preprocess_sentiment(sentence):
        # lowercase
        sentence = sentence.lower()
        # remove special characters: {!,@#$%^ *()~;:/<>\|+_-[]?}
        sentence = sentence.translate ({ord(c): " " for c in "!@#$%^&*()[]{};:,./<>?\|`~-=_+"})
        # tokenize
        sentence = word_tokenize(sentence)
        # exclude common word
        sentence = [word for word in sentence if not word in stopwords.words()]
        # combine back to a sentence
        sentence = ' '.join(sentence)
        # sentiment
        sentiment = TextBlob(sentence).sentiment
        # sentiment score
        # sentiment_score = sentiment[0] * (1-sentiment[1])
        return sentiment

    def sentiments_scores():
        file = self.file
        sentiments = []
        scores = []
        for comment in file['content']:
            sentiment = preprocess_sentiment(comment)
            score = sentiment[0] * (1 - sentiment[1])
            sentiments.append(sentiment)
            scores.append(score)
            # print(sentiment)
            # print(score)
        return sentiments, scores

    def average_score():
        average_scores = []
        for i in range(0, len(scores)):
            average_score = sum(scores[i:])/len(scores[i:])
            average_scores.append(average_score)
        df['average_score'] = average_scores

