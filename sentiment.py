import nltk
# nltk.download('stopwords')
# nltk.download('punkt')
# nltk.download('vader_lexicon')
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.sentiment.vader import SentimentIntensityAnalyzer # calculate sentiment score
from textblob import TextBlob # calculate the subjective
import pickle
import os
import numpy as np

def preprocess_sentiment(sentence):

    # sentiment from textblob
    TextBlob_sentiment = TextBlob(sentence).sentiment

    # sentiment from vader
    sid = SentimentIntensityAnalyzer()
    vader_sentiment = sid.polarity_scores(sentence)

    return TextBlob_sentiment[0], TextBlob_sentiment[1], vader_sentiment['neg'], vader_sentiment['neu'], vader_sentiment['compound']

# loop through all files in googleplay_scrape and check whether file in snetimnet_calculated
for subdir, dirs, files in os.walk("./docs/googleplay_scrape_df"):
    for file in files:
        filepath = subdir + os.sep + file
        with open(filepath, "rb") as fp:
            df = pickle.load(fp)
        print(df)

        # edge
        if 'textblob_polarity' not in df.columns:
            df['textblob_polarity'] = None
        if 'textblob_subjectivity' not in df.columns:
            df['textblob_subjectivity'] = None
        if 'vader_score_neg' not in df.columns:
            df['vader_score_neg'] = None
        if 'vader_score_neu' not in df.columns:
            df['vader_score_neu'] = None
        if 'vader_score_compound' not in df.columns:
            df['vader_score_compound'] = None
        if 'vader_score_compound_average' not in df.columns:
            df['vader_score_compound_average'] = None

        # calculate sentiment score
        for index, row in df.iterrows():
            if (row['textblob_polarity'] is None) or (row['textblob_subjectivity'] is None) or (row['vader_score_neg'] is None) or (row['vader_score_neu'] is None) or (row['vader_score_compound'] is None) or (row['vader_score_compound_average'] is None):
                print("calculating sentiment ", file, row['at'])
                df.iloc[index, df.columns.get_loc('textblob_polarity')] = preprocess_sentiment(row['content'])[0]
                df.iloc[index, df.columns.get_loc('textblob_subjectivity')] = preprocess_sentiment(row['content'])[1]
                df.iloc[index, df.columns.get_loc('vader_score_neg')] = preprocess_sentiment(row['content'])[2]
                df.iloc[index, df.columns.get_loc('vader_score_neu')] = preprocess_sentiment(row['content'])[3]
                df.iloc[index, df.columns.get_loc('vader_score_compound')] = preprocess_sentiment(row['content'])[4]
                df_sub = df[:index]
                # print(df_sub)
                df.iloc[index, df.columns.get_loc('vader_score_compound_average')] = df_sub['vader_score_compound'].mean()
                with open(filepath, "wb") as fp:
                    pickle.dump(df, fp)
            else:
                pass
