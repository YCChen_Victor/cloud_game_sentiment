import os
import plotly.graph_objs as go
import pickle

# read dfs
dfs = {}
for subdir, dirs, files in os.walk("./docs/googleplay_scrape_df"):
    for file in files:
        # print(os.path.join(subdir, file))
        filepath = subdir + os.sep + file
        with open(filepath, "rb") as fp:
            df = pickle.load(fp)
        dfs[file] = df

# timeseries plot ####
df_names = [key.split('.')[0] for key, value in dfs.items()]
df_names = [key.replace(' ', '') for key in df_names]
dfs = [value for key, value in dfs.items()]
dfs_rename = {}
for i in range(len(dfs)): # rename the colnames
    df_name = df_names[i]
    df = dfs[i]
    name = {}
    for colname in df.columns:
        if colname == 'at':
            continue
        name[colname] = colname + '_' + df_names[i]
    df = df.rename(columns=name)
    dfs_rename[df_name] = df
total_fig = go.Figure()
for key, item in dfs_rename.items():
    try:
        total_fig.add_trace(
            go.Scatter(x=item['at'], y=item['vader_score_compound_average' + '_' + key],
                mode='lines',
                name=key))
    except:
        pass

with open("./docs/sentiment_timeseries_plot.txt", "wb") as fp:
    pickle.dump(total_fig, fp)
