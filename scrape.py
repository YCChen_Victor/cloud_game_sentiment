from google_play_scraper import Sort, reviews
import pprint
import pandas as pd
import pickle
'''
1.'reviewId': 不知道是什麼
2.'userName': 就是用戶的名稱
3.'userImage': 用戶的頭像
4.'content': 貼文內容
5.'score': 用戶打的分數
6.'thumbsUpCount': 大家給他按讚數
7.'reviewCreatedVersion':不知道是什麼
8.'at': 什麼時候回覆
9.'replyContent': 下面的人回覆他什麼
10.'repliedAt': 下面的人什麼時候回覆
'''

with open("docs/website_to_scrape.txt", "rb") as fp:
    website = pickle.load(fp)
print(website)

#scrape from google play
result, _ = reviews(
    website,
    lang='en', # defaults to 'en'
    country='us', # defaults to 'us'
    sort=Sort.NEWEST, # defaults to Sort.MOST_RELEVANT
    count=10000000, # defaults to 100
    filter_score_with=None, # defaults to None(means all score)
    continuation_token=None # defaults to None(load from the beginning)
)
# save file to local directory
df_result = pd.DataFrame(result)
df_result = df_result.reindex(index=df_result.index[::-1])
df_result = df_result.reset_index()
name = website.replace(".", "_")
with open("docs/googleplay_scrape_df/ {}.txt".format(name), "wb") as fp:
    pickle.dump(df_result, fp)
