import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
from dash.dependencies import Input, Output, State

import os
import pickle
import pandas as pd
from functools import reduce

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
app.title = "Google Play Sentiment Analysis"

# layout
app.layout = html.Div([
    html.H1("Google Play Sentiment Analysis"),
    html.Div([
        html.H6("1. 輸入預計要 SCRAPE 的網頁，ex: com.google.stadia.android；輸入完後，點選 SCRAPE BUTTON"),
        dcc.Input(
            id="input_text",
            type="text",
            placeholder="input website",
        ),
        html.Button(
            'Scrape',
            id='submit-val',
            n_clicks=0
        ),
        html.Div(
            id="whether_comment_saved",
        ),
    ]),
    html.Div([
        html.H6("2. 點選 CALCULATE SENTIMENTS BUTTON，將會開始計算資料夾中的 dataframe 的 sentiment scores"),
        html.Div(
            id='whether_sentiment_calculated'
            ),
        html.Div(
            id='whether_website_input'
            ),
        html.Button(
            'calculate sentiments',
            id='calculate_sentiments',
            n_clicks=0),
        html.P("Sentiment All Calculated: "),
        html.Ul(
            id="sentiment_calculated",
            ),
        html.P(
            "Still Exists Sentiment Unalculated: "
            ),
        html.Ul(
            id="sentiment_uncalculated",
            ),
        ]),
    html.Div([
        html.H6(
            "3. 點選 PLOT BUTTON，將會畫出資料夾中 dataframe 的 average sentiment scores"
            ),
        html.Button(
            'Plot',
            id='submit_plot',
            n_clicks=0
            ),
        dcc.Graph(
            id="average_sentiment_plot"
            )
        ]),
    html.Div([
        html.H6(
            "4. 顯示歷史 Comments"
            ),
        ])
    ])

#  after click
@app.callback(
    Output("whether_website_input", "children"),
    [Input("input_text", "value")],
)
def comment_scrape(text):
    filepath = './docs/website_to_scrape.txt'
    with open(filepath, "wb") as fp:
        pickle.dump(text, fp)
    return [None]

# scrape after click
@app.callback(
    Output("whether_comment_saved", "children"),
    [Input("submit-val", "n_clicks")],
)
def comment_scrape(n_click):
    if n_click != 0:
        import scrape
        return "scraped"
    else:
        return "no input, please input something like com.google.stadia.android"

# calculate sentiment after click
@app.callback(
    Output("whether_sentiment_calculated", "children"),
    [Input("calculate_sentiments", "n_clicks")],
)
def calculate_sentiment(n_click):
    if n_click != 0:
        import sentiment
        return "sentiment calculated"
    else:
        return "initializing"

# trigger calculate sentiment score ####
@app.callback(
    Output("average_sentiment_plot", "figure"),
    [Input("submit_plot", "n_clicks")],
)
def sentiments_timeseries_plot(n_click):
    # load dataframes
    '''
    1.'reviewId': 不知道是什麼
    2.'userName': 就是用戶的名稱
    3.'userImage': 用戶的頭像
    4.'content': 貼文內容
    5.'score': 用戶打的分數
    6.'thumbsUpCount': 大家給他按讚數
    7.'reviewCreatedVersion':不知道是什麼
    8.'at': 什麼時候回覆
    9.'replyContent': 下面的人回覆他什麼
    10.'repliedAt': 下面的人什麼時候回覆
    '''
    if n_click != 0:
        import sentiment_timeseries_plot
        with open("./docs/sentiment_timeseries_plot.txt", "rb") as fp:
            plot = pickle.load(fp)
        return plot
    else:
        plot = go.Figure()
        return plot

# check whether sentiment calculated
@app.callback(
    [Output("sentiment_calculated", "children"),
    Output("sentiment_uncalculated", "children")],
    [Input("calculate_sentiments", "n_clicks")],
)
def sentiments_timeseries_plot(n_click):
    for subdir, dirs, files in os.walk("./docs/googleplay_scrape_df"):
        sentiment_calculated_file = []
        sentiment_uncalculated_file = []
        for file in files:
            # print(os.path.join(subdir, file))
            filepath = subdir + os.sep + file
            with open(filepath, "rb") as fp:
                df = pickle.load(fp)
            if 'vader_score_compound_average' not in df.columns:
                sentiment_uncalculated_file.append(file)
            elif None in df['vader_score_compound_average'].values:
                sentiment_uncalculated_file.append(file)
            else:
                sentiment_calculated_file.append(file)
        if len(sentiment_calculated_file) == 0:
            sentiment_calculated_file = ["none"]
        return ([html.Li(i) for i in sentiment_calculated_file],
               [html.Li(i) for i in sentiment_uncalculated_file])


if __name__ == '__main__':
    app.run_server(debug=True, port=8051)
